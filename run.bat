docker build -t opencvbuild:4.5.5 --build-arg OPENCV_VERSION=4.5.5 .
docker rm builder
docker run --name builder --attach STDOUT opencvbuild:4.5.5

del project/opencv

docker container cp builder:/root/build/install/sdk project/opencv

cd project

gradlew clean
gradlew opencv:assembleRelease

cp .\opencv\build\outputs\aar\opencv-release.aar ../

cd ..