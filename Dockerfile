FROM openjdk:11-jdk-slim-bullseye
WORKDIR /root

RUN apt-get update && \
    apt-get install -y --no-install-recommends cmake ninja-build git wget unzip python3 bash && \
    rm -rf /var/lib/apt/lists/*
RUN wget -nv https://dl.google.com/android/repository/commandlinetools-linux-6858069_latest.zip && \
    unzip -q commandlinetools-linux-6858069_latest.zip && \
    yes | cmdline-tools/bin/sdkmanager --sdk_root=/opt/android_sdk/ "platform-tools" "platforms;android-30" "build-tools;30.0.3" && \
    wget -nv https://dl.google.com/android/repository/android-ndk-r21e-linux-x86_64.zip && \
    unzip -q android-ndk-r21e-linux-x86_64.zip -d /opt/ && \
    rm *.zip

# By default we build the currently latest opencv, but can be changed from command line by using --build-arg opencv_version=4.5.5 for example
ARG OPENCV_VERSION=4.6.0
# I separated the android setup and the OpenCV checkout, so the android setup can be reused with newer opencv versions
RUN git clone --branch $OPENCV_VERSION https://github.com/opencv/opencv && \
    git clone --branch $OPENCV_VERSION https://github.com/opencv/opencv_contrib
    

ENV ANDROID_NDK=/opt/android-ndk-r21e/ 
ENV ANDROID_SDK=/opt/android_sdk/
ENV ANDROID_HOME=/opt/android_sdk/
ENV ANDROID_NDK_HOME=/opt/android-ndk-r21e/
ENV ANDROID_PLATFORM=30

WORKDIR /root/build
COPY build_all.sh build_opencv_slim.sh /root/build/

SHELL ["/bin/bash", "-c"]
CMD ["/root/build/build_all.sh", "build_opencv_slim.sh"]

